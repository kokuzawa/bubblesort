package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Bubble Sort in Java FX.
 * @author Katsumi
 * @since Sep 5, 2013
 */
public class Main extends Application
{
    private static final int N = 100;
    private static final int VIEW_HEIGHT = (N * 2) + 20;
    private final List<Integer> data = new ArrayList<>();
    private final Path path = new Path();
    private final Path currentPath = new Path();
    private final Group root = new Group();
    private final Timeline timeline = new Timeline();
    private boolean flag = false;
    private int counter = 0;
    private int k = 0;

    /**
     * パスを再描画します。
     * @return ラインの最終描画ポジション
     */
    int repaint()
    {
        path.getElements().clear();
        currentPath.getElements().clear();
        int position = 10;
        int localCounter = 0;
        for (Integer i : data) {
            if (counter == localCounter) {
                currentPath.getElements().add(new MoveTo(position, (VIEW_HEIGHT - 10)));
                currentPath.getElements().add(new LineTo(position, ((VIEW_HEIGHT - 10) - (i * 2))));
            }
            else {
                path.getElements().add(new MoveTo(position, (VIEW_HEIGHT - 10)));
                path.getElements().add(new LineTo(position, ((VIEW_HEIGHT - 10) - (i * 2))));
            }
            position += 5;
            localCounter++;
        }

        return position;
    }

    /**
     * Bubble Sort のソート過程を描画します。
     */
    void play()
    {
        timeline.getKeyFrames().addAll(
                new KeyFrame(new Duration(10), new EventHandler<ActionEvent>()
                {
                    @Override
                    public void handle(ActionEvent actionEvent)
                    {
                        if (data.get(counter) > data.get(counter + 1)) {
                            flag = true;
                            Collections.swap(data, counter, counter + 1);
                        }

                        counter++;

                        repaint();

                        if (counter >= N - 1 - k) {
                            if (!flag) {
                                timeline.stop();
                            }
                            flag = false;
                            counter = 0;
                            k++;
                        }
                    }
                })
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
     * ソート対象データを初期化します。
     */
    void initData()
    {
        for (int i = 1; i <= N; i++) {
            data.add(i);
        }
        Collections.shuffle(data);
    }

    /**
     * ステージを初期化します。
     * @param primaryStage ステージ
     */
    void initStage(Stage primaryStage)
    {
        path.setStrokeWidth(3.0);
        path.setStroke(Color.GRAY);
        currentPath.setStrokeWidth(4.0);
        currentPath.setStroke(Color.RED);
        root.getChildren().add(path);
        root.getChildren().add(currentPath);

        final int width = repaint();

        primaryStage.setTitle("Bubble Sort");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, width + 10, VIEW_HEIGHT));
        primaryStage.show();
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        initData();
        initStage(primaryStage);
        play();
    }

    public static void main(String... args)
    {
        launch(args);
    }
}
